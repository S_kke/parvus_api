<?php
function get_from_database() {
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "db";

    $conn = new mysqli($servername, $username, $password);

    $selected_db = mysqli_select_db($conn, $dbname);

    if (!$selected_db) {
        $query = "CREATE DATABASE db;";
        $conn->query($query);
    } 

    $query = 'SELECT 1 FROM api_data;';

    if (empty($conn->query($query))) {
        $query = "CREATE TABLE api_data (time timestamp, amount text)";
        $conn->query($query);
    }

    $query = 'SELECT * FROM api_data ORDER BY time DESC LIMIT 1;';

    $result = $conn->query($query);

    while($row = mysqli_fetch_array($result))
    {
        $db_time = $row[0];
        $db_amount = $row[1];
    }

    $values = array($db_time, $db_amount);

    return $values;
}

function update_database($servername, $username, $password, $dbname) {
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "db";

    $ch = curl_init();
    $headers = array(
        'Api-Token: xxx',
        "Content-type: application/json", 
        "Accept: application/json"
    );

    $conn = new mysqli($servername, $username, $password, $dbname);

    curl_setopt($ch, CURLOPT_URL, "https://muutosinnovations.api-us1.com/api/3/contacts?listid=41&status=1");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $data = json_decode(curl_exec($ch), true);

    curl_close($ch);

    $contact_count = $data['meta']['total'];
    $amount = $contact_count * 0.35;
    $timestamp = date("Y-m-d H:i:s");

    $query = "INSERT INTO api_data VALUES ('$timestamp', '$amount')";

    $conn->query($query);

    $conn->close();

    $values = array($timestamp, $amount);

    return $values;
}

?>

